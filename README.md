# Game Idea #1

## Distro-roids

Two distro themed circles float from left to right. Use the controls
to move around the play area and shoot the distro circles. As they are
shot, their hit points are reduced. The new, weaker distribution will
then take it's place. Once the weakest distro is destroyoed, you 
score a point.

## Controls
- Right Arrow = Rotate Clockwise
- Left Arrow = Rotate Counter Clockwise
- Up Arrow = Accelerate
- Space = Fire

[Distro-roids Hosted by Gitlab](https://gginorio.gitlab.io/game-idea/)

![Screenshot of Distro-roids](public/assets/images/screenshot.png)