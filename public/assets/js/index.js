
import Board  from './board.js';
import Ship   from './ship.js';
import Aliens from './aliens.js';

const board = new Board('board');
if (board.create()) {

    const ship   = new Ship('ship');
    const aliens = new Aliens(board);
    ship.setAliens(aliens);

    if (ship.create(board)) {
        aliens.create(2);
    }
}