
class Board {
    constructor(boardId)
    {
        this.backgroundURI = document.URL + 'assets/images/backgrounds';
        
        this.body    = document.querySelector('body');
        this.element = document.getElementById(boardId);
    }

    setBoardElement(boardId)
    {
        this.element = document.getElementById(boardId);
    }

    setBackgroundURI(uri)
    {
        this.backgroundURI = document.URL + uri;
    }

    updateBackgroundImage()
    {
        const imageName = '0' + Math.ceil(Math.random() * 5);
        const image = `${this.backgroundURI}/${imageName}.jpg`;

        this.body.style.background = `url(${image})`;
    }

    create()
    {
        if (this.element == null) {
            return false;
        }

        this.updateBackgroundImage();

        return true;
    }

    getBoundries()
    {
        return {
            left   : 0,
            right  : this.element.offsetWidth,
            top    : 0,
            bottom : this.element.offsetHeight
        }
    }
}

export default Board;