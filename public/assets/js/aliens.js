
class Aliens 
{
    constructor(board)
    {
        this.intervals    = [];
        this.milliseconds = 70;

        this.board = board;
        this.boundries = this.board.getBoundries();
        
        this.icons = {
            explode: {
                image: "assets/images/explode.svg",
                power: 0
            },
            alma: {
                image: "assets/images/aliens/alma.svg",
                power: 3
            },
            arch: {
                image: "assets/images/aliens/arch.svg",
                power: 8
            },
            debian: {
                image: "assets/images/aliens/debian.svg",
                power: 2
            },
            endeavour: {
                image: "assets/images/aliens/endeavour.svg",
                power: 1
            },
            manjaro: {
                image: "assets/images/aliens/manjaro.svg",
                power: 7
            },
            mint: {
                image: "assets/images/aliens/mint.svg",
                power: 6
            },
            redhat: {
                image: "assets/images/aliens/redhat.svg",
                power: 5
            },
            ubuntu: {
                image: "assets/images/aliens/ubuntu.svg",
                power: 4
            }
        }

        this.active = [];
    }

    create(count)
    {
        for (let i = 0; i < parseInt(count); i++) {
            let element = this.createIcon();
            this.activateIcon(element);
        }

        return true;
    }

    createIcon()
    {
        const keys    = Object.keys(this.icons);
        const random  = Math.floor(Math.random() * keys.length);
        const icon    = this.icons[keys[(random == 0 ? 1 : random)]];
        
        const element = document.createElement('img');
        element.src   = icon.image;
        element.classList.add('alien');
        element.dataset.power = icon.power;
        element.dataset.x = 0;
        element.dataset.y = 0;

        const left = this.board.element.offsetWidth - 50;
        const top  = Math.floor(Math.random() * (this.board.element.offsetHeight - 50));
        
        element.dataset.left = left;
        element.dataset.top  = top;

        element.style.left = `${left}px`;
        element.style.top  = `${top}px`;

        this.board.element.append(element);
        
        this.active.push(element);

        return element;
    }

    activateIcon(element)
    {
        const speed  = (Math.random() * 2) + 1;
        const rotate = 315 - Math.floor(Math.random() * 90);
        const ratio = (270 - rotate) / 45;
        
        const x = -Math.round(Math.abs(speed - (speed * ratio)) * 10) / 10;
        const y = Math.round(speed * ratio * 10) / 10;

        element.dataset.x = x;
        element.dataset.y = y;

        this.accelerate(element);
    }

    accelerate(element)
    {
        const interval = setInterval(() => {
            this.updatePosition(element);
        }, this.milliseconds);

        this.intervals.push(interval);
    }

    updatePosition(element)
    {
        const left = parseInt(element.dataset.left) + parseFloat(element.dataset.x);
        const top  = parseInt(element.dataset.top)  + parseFloat(element.dataset.y);
        
        element.style.left   = `${left}px`;
        element.style.top    = `${top}px`;

        element.dataset.left = left;
        element.dataset.top  = top;

        this.checkBoundries(element);
    }

    checkBoundries(element)
    {
        const left = parseFloat(element.dataset.left);
        const top  = parseFloat(element.dataset.top);

        if (top <= this.boundries.top || top + element.height >= this.boundries.bottom) {
            element.dataset.y = -1 * (element.dataset.y);
        }

        if (left <= this.boundries.left - 20) {
            element.style.left   = `${this.boundries.left}px`;
            element.dataset.left = this.boundries.left;

            const interval = this.intervals[this.active.indexOf(element)];
            clearInterval(interval);
            this.intervals = this.intervals.filter(current => current !== interval);
            
            this.active = this.active.filter(alien => alien !== element);
            this.board.element.removeChild(element);

            let icon = this.createIcon();
            this.activateIcon(icon);
        }
    }

    hit(element)
    {
        const power = element.dataset.power;
        let score   = false;

        if (power == 0) {
            return false;
        
        } else if (power == 1) {
            element.classList.add('explode');
            
            setTimeout(() => {
                this.active = this.active.filter(alien => element !== alien);
                element.parentNode.removeChild(element);
                this.create(1);
            }, 1000);

            score = true;
        }

        const icon = Object.values(this.icons).filter(icon => icon.power == power - 1);
        if (icon.length) {
            element.src = icon[0].image;
            element.dataset.power = icon[0].power;
        }

        return score;
    }
}

export default Aliens;
