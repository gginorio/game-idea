
import Board from './board.js';
import Aliens from './aliens.js';

class Ship 
{
    constructor(shipId) 
    { 
        this.board    = null;
        this.aliens   = null;
        this.timer    = null;

        this.element  = document.getElementById(shipId);

        this.icons    = {
            normal : document.URL + 'assets/images/pieces/ship.svg',
            active : document.URL + 'assets/images/pieces/ship.rocket.svg',
            explode: document.URL + 'assets/images/explode.svg'
        }

        this.fireURI  = 'assets/images/aliens/arch.svg';

        this.position =  {
            left: 0,
            top : 0
        }

        this.acceleration = {
            x: 0,
            y: 0
        }
    }

    setBoard(board)
    {
        if (!(board instanceof Board)) {
            return false;
        
        } else if (board.element == null) {
            return false;
        
        }

        this.board = board;
        
        return true;
    }

    setAliens(aliens)
    {
        if (!(aliens instanceof Aliens)) {
            return false;
        }

        this.aliens = aliens;
        
        return true;
    }

    setElement(id) 
    {
        const element = document.getElementsById(id);
        if (element == null || element.tagName !== 'IMG') {
            return false;
        }

        this.element = element;

        return true;
    }

    setIcons(icons) 
    {
        if (typeof icons !== 'object' || Object.keys(icons).length !== 2) {
            return false;
        
        } else if (!Object.keys(icons).includes('normal') || !Object.keys(icons).includes('active')) {
            return false;
        }

        this.icons = icons;

        return true;
    }

    setPosition(positions) 
    {
        if (typeof positions !== 'object' || Object.keys(positions) !== 2) {
            return false;
        
        } else if (!Object.keys(icons).includes('left') || !Object.keys(icons).includes('top')) {
            return false;
        }

        this.position = positions;

        return true;
    }

    create(board)
    {
        if (!this.setBoard(board)) {
            alert('Board passed in is not a valid instance of the Board.js class with a valid HTML element as a container');
            return false;
        }

        this.addToBoard();
        this.activateControls();

        return true;
    }

    addToBoard()
    {
        this.element = document.createElement('img');
        this.element.id  = 'ship';
        this.element.dataset.rotation = 0;
        this.element.src = this.icons.normal;

        this.board.element.appendChild(this.element);
    }

    activateControls()
    {
        document.addEventListener('keydown', (event) => {
            switch (event.key) {
                
                case 'ArrowRight':
                    this.rotate(10);
                    break;
                
                case 'ArrowLeft':
                    this.rotate(-10);
                    break;
                
                case 'ArrowUp':
                    this.accellerate(1);
                    break;
                
                case "ArrowDown":
                    // this.accellerate(-6);
                    break;
                
                case " ":
                    this.fire();
                    break;

                default: break;
            }
        });
    }

    rotate(degrees)
    {
        let rotation = parseInt(this.element.dataset.rotation) + degrees;
        rotation = rotation == 360 ? 0 : rotation;
        rotation = rotation < 0 ? 360 + degrees : rotation;

        this.element.style.transform  = `rotate(${rotation}deg)`;
        this.element.dataset.rotation = rotation;

        this.updateMeters();
    }

    accellerate(movement)
    {
        this.showAccelerateIcon();
        this.calculateMotion(movement);
       
        if (this.timer) {
            clearInterval(this.timer);
        
        } else if (this.acceleration.x == 0 && this.acceleration.y == 0) {
            clearInterval(this.timer);
        }

        this.timer = setInterval(() => {
            this.updatePosition();
            this.updateMeters();
        }, 10);
    }

    showAccelerateIcon()
    {
        this.element.src = this.icons.active;
        setTimeout(() => {
            this.element.src = this.icons.normal;
        }, 500);
    }

    calculateMotion(movement)
    {
        const rotation = this.element.dataset.rotation;
        const move     = movement * ((rotation % 90) / 80);

        if (rotation < 90) {
            this.acceleration.y -= movement - move;
            this.acceleration.x += move;
        
        } else if (rotation < 180) {
            this.acceleration.y += move;
            this.acceleration.x += movement - move;

        } else if (rotation < 270) {
            this.acceleration.y += movement - move;
            this.acceleration.x -= move;

        } else {
            this.acceleration.y -= move;
            this.acceleration.x -= movement - move;
        }
    }

    updatePosition()
    {
        this.position.left += parseFloat(this.acceleration.x);
        this.position.top  += parseFloat(this.acceleration.y);

        this.boundryCheck();

        this.element.style.left = `${this.position.left}px`;
        this.element.style.top  = `${this.position.top}px`;

        if (this.isHit(this.position.left, this.position.top, this.element.width)) {
            this.element.src = this.icons.explode;
            this.element.classList.add('explode');

            setTimeout(() => {
                this.element.parentNode.removeChild(this.element);
                this.acceleration.x = 0;
                this.acceleration.y = 0;
                this.addToBoard();
            }, 1000);
        }
    }

    boundryCheck()
    {
        const ship      = this.element;
        const positions = this.position;
        
        const boundries   = this.board.getBoundries();
        boundries.right  -= ship.width;
        boundries.bottom -= ship.width;
        
        if (positions.left < boundries.left || positions.left > boundries.right) {
            this.acceleration.x = 0;
        }

        if (positions.top < boundries.top || positions.top > boundries.bottom) {
            this.acceleration.y = 0
        }

        positions.left = positions.left < boundries.left ? boundries.left : positions.left;
        positions.top  = positions.top  < boundries.top  ? boundries.top  : positions.top;

        positions.left = positions.left > boundries.right  ? boundries.right  : positions.left;
        positions.top  = positions.top  > boundries.bottom ? boundries.bottom : positions.top;

        this.position.left = positions.left;
        this.position.top  = positions.top;
    }

    updateMeters()
    {
        const combinedSpeed = Math.round((Math.abs(this.acceleration.x) + Math.abs(this.acceleration.y)) * 10) / 10;
        const relativeSpeed = Math.round((this.acceleration.x + this.acceleration.y) * 10) / 10;

        document.querySelector('#speed').innerHTML = combinedSpeed * 10;
        document.querySelector('#speed-meter').style.left = relativeSpeed + "%";

        document.querySelector('#rotation').innerHTML = this.element.dataset.rotation;
        document.querySelector('#rotation-meter').style.left = (((this.element.dataset.rotation - 180) / 360) * 100) + "%";
    }

    fire()
    {
        const boundries = this.board.getBoundries();
        const img = document.createElement('img');
        img.src   = this.fireURI;
        
        const movement = this.calculateFireMotion(5, this.element.dataset.rotation);
        img.dataset.x  = movement.x;
        img.dataset.y  = movement.y;

        img.style.left = `${this.position.left + 10}px`;
        img.style.top  = `${this.position.top + 10}px`;
        img.classList.add('shot');

        this.board.element.appendChild(img);
        
        const interval = setInterval(() => {
            let left = img.offsetLeft + parseFloat(img.dataset.x);
            let top  = img.offsetTop  + parseFloat(img.dataset.y);
            
            img.style.left = `${left}px`;
            img.style.top  = `${top}px`;

            if (left < boundries.left || 
                left > boundries.right - 20 || 
                top  < boundries.top || 
                top  > boundries.bottom - 20
            ) {
                img.parentNode.removeChild(img);
                clearInterval(interval);
            
            } else if (this.isHit(left, top, img.width)) {
                img.parentNode.removeChild(img);
                clearInterval(interval);
            }

        }, 10);
    }

    calculateFireMotion(movement, rotation)
    {
        const move = movement * ((rotation % 90) / 80);
        const acceleration = { x: 0, y: 0 };

        if (rotation < 90) {
            acceleration.y -= movement - move;
            acceleration.x += move;
        
        } else if (rotation < 180) {
            acceleration.y += move;
            acceleration.x += movement - move;

        } else if (rotation < 270) {
            acceleration.y += movement - move;
            acceleration.x -= move;

        } else {
            acceleration.y -= move;
            acceleration.x -= movement - move;
        }

        return acceleration;
    }

    isHit(left, top, width)
    {
        let alien = this.aliens.active.filter((alien) => {
            return Math.abs(alien.offsetLeft - left) < width && Math.abs(alien.offsetTop - top) < width;
        });

        if (alien.length) {
            if (this.aliens.hit(alien[0])) {
                const scoreElement     = document.getElementById('score');
                scoreElement.innerHTML = parseInt(scoreElement.innerHTML) + 1;
                const scoreMeter       = document.getElementById('score-meter');
                scoreMeter.style.left  = (parseInt(scoreElement.innerHTML) + 1) + "px";
            }
            return true;
        }

        return false;
    }
}

export default Ship;